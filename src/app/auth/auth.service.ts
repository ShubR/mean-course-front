import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthData } from './auth-data.model';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { takeWhile } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  private isAuthenticated: boolean = false;
  private baseUrl = 'http://localhost:8000/api/user';
  private token: string = '';
  private authStatusListener = new Subject<boolean>();
  private timerIsOn: boolean = false;
  private userId: string | null = '';
  // private tokenTimer = NodeJs.Timer;

  getToken = (): string => this.token;

  getUserId = (): string  | null => this.userId;

  getIsAuth = (): boolean => this.isAuthenticated;

  getAuthStatusListner = () => this.authStatusListener.asObservable();

  createUser(signupData: AuthData) {
    this.http.post(`${this.baseUrl}/signup`, signupData).subscribe((res) => {});
  }

  loginUser(loginData: AuthData) {
    this.http
      .post<{ token: string; expiresIn: number; userId: string }>(
        `${this.baseUrl}/login`,
        loginData
      )
      .subscribe((res) => {
        const token = res.token;
        if (token) {
          const expiresIn: number = res.expiresIn;

          this.setAuthTimer(expiresIn);

          this.isAuthenticated = true;
          this.token = token;
          this.userId = res.userId;
          this.authStatusListener.next(true);

          const now = new Date();

          const expirationDate = new Date(now.getTime() + expiresIn * 1000);

          this.saveAuthData(token, expirationDate, this.userId);

          this.router.navigate(['/']);
        }
      });
  }

  autoAuthUser() {
    const authInfo = this.getAuthData();
    if (authInfo?.isInFuture) {
      this.token = authInfo.token;
      this.isAuthenticated = true;
      this.userId = authInfo.userId;
      this.authStatusListener.next(true);


      const expiresIn =
        authInfo.expirationDate.getTime() - new Date().getTime();

      this.setAuthTimer(expiresIn / 1000);
    } else if (authInfo?.token) {
      this.logout();
    }
  }

  logout() {
    this.timerIsOn = false;
    this.token = '';
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    this.userId = '';
    this.clearAuthData();
    this.router.navigate(['/']);
  }

  private saveAuthData(token: string, expirationDate: Date, userId: string) {
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString());
    localStorage.setItem('userId', userId);
  }

  private getAuthData() {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    const userId = localStorage.getItem('userId');

    if (!token || !expirationDate) {
      return;
    }

    const now = new Date();

    const isInFuture = new Date(expirationDate) > now;

    return {
      token: token,
      expirationDate: new Date(expirationDate),
      isInFuture: isInFuture,
      userId: userId,
    };
  }

  private setAuthTimer(duration: number) {
    this.timerIsOn = true;

    console.log('setting timer :: ', duration);

    timer(duration * 1000)
      .pipe(takeWhile(() => this.timerIsOn))
      .subscribe(() => {
        this.logout();
      });
  }

  private clearAuthData() {
    localStorage.clear();
  }
}
