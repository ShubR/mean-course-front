export interface Post {
  title: string;
  content: string;
  _id?: string;
  imagePath?: string;
  creator?: string;
}
