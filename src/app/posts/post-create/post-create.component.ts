import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from '../post.model';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss'],
})
export class PostCreateComponent implements OnInit {
  enteredContent: string = '';
  enteredTitle: string = '';
  newPost: string = 'No content';
  private mode: string = 'create';
  postId: string = '';

  imgUrl: string | undefined = '';

  imgFile!: File;

  constructor(
    private postservice: PostsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.postId = this.route.snapshot.params['id'];

    if (this.postId) {
      this.mode = 'edit';
      this.getSinglePost(this.postId);
    } else {
      this.mode = 'create';
      this.postId = '';
    }
  }

  onImageSelect(event: Event) {
    const targetEvent = event.target as HTMLInputElement;
    if (targetEvent.files && targetEvent.files[0]) {
      this.imgFile = targetEvent.files[0];

      const reader = new FileReader();

      reader.readAsDataURL(targetEvent.files[0]);
      reader.onloadend = (e: any) => {
        this.imgUrl = e.target['result'];

        console.log('fileurl :: ', typeof this.imgUrl);
      };
    }
  }

  onSavePost() {
    const post: Post = {
      title: this.enteredTitle,
      content: this.enteredContent,
    };

    if (this.mode === 'create') {
      this.postservice.addPost(post, this.imgFile);
    } else {
      let img: File | string | undefined = this.imgFile
        ? this.imgFile
        : this.imgUrl;

      this.postservice.updatePost(this.postId, post, img).subscribe(
        (res) => {
          this.router.navigate(['/']);
        },
        (err) => console.error('Error on update post :: ', err)
      );
    }

    this.enteredContent = '';
    this.enteredTitle = '';
  }

  getSinglePost(id: string) {
    this.postservice.getPostById(id).subscribe((res) => {
      this.enteredTitle = res.post.title;
      this.enteredContent = res.post.content;
      this.imgUrl = res.post.imagePath;
    });
  }
}
