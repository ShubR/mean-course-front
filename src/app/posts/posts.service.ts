import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Post } from './post.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  baseUrl = 'http://localhost:8000';
  updatePosts = new Subject();

  constructor(private http: HttpClient, private router: Router) {}

  getPosts = (currentPage: number, postsPerPage:number,) => {
    const queryParams:string = `?page=${currentPage}&pagesize=${postsPerPage}`
    return this.http.get<{ message: string; posts: Array<any>; id: string }>(
      `${this.baseUrl}/api/posts${queryParams}`
    );
  };

  addPost(post: Post, image: File) {
    const postData = new FormData();
    postData.append('title', post.title);
    postData.append('content', post.content);
    postData.append('image', image, post.title);

    this.http.post<any>(`${this.baseUrl}/api/posts`, postData).subscribe(
      (res) => this.router.navigate(['/']),
      (err) => console.error('Error on add post ', err)
    );
  }

  deletePost(id: string | undefined) {
    this.http
      .delete<{ message: string }>(`${this.baseUrl}/api/posts/${id}`)
      .subscribe(
        (res) => this.updatePosts.next(),
        (error: any) => console.error('Error on delete post :: ', error)
      );
  }

  getPostById = (id: string) =>
    this.http.get<{ message: string; post: Post }>(
      `${this.baseUrl}/api/posts/${id}`
    );

  updatePost(id: string, data: Post, image: File | string | undefined) {
    let postData;

    if (typeof image === 'object') {
      postData = new FormData();
      postData.append('title', data.title);
      postData.append('content', data.content);
      postData.append('image', image, data.title);
    } else {
      postData = { title: data.title, content: data.content, imagePath: image };
    }

    return this.http.put<any>(`${this.baseUrl}/api/posts/${id}`, postData);
  }
}
