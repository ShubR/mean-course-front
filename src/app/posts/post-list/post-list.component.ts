import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Post } from '../post.model';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
})
export class PostListComponent implements OnInit, OnDestroy {
  @Input() posts: Array<Post> = [];

  totalPosts: number | undefined;
  postsPerPage = 2;
  currentPage = 1;
  pageSizeOptions = [1, 2, 5, 10];

  userIsAuthenticated: boolean = false;

  userId: string | null = '';

  private authSubscribtion: Subscription | undefined;

  constructor(
    private postsService: PostsService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.userIsAuthenticated = this.authService.getIsAuth();

    this.getPosts(this.currentPage, this.postsPerPage);

    this.userId = this.authService.getUserId();

    this.postsService.updatePosts.subscribe(() => {
      this.getPosts(this.currentPage, this.postsPerPage);
    });

    this.authSubscribtion = this.authService
      .getAuthStatusListner()
      .subscribe((isAuth) => {
        this.userIsAuthenticated = isAuth;
        this.userId = this.authService.getUserId();
      });
  }

  onDelete(id: string | undefined) {
    this.postsService.deletePost(id);
  }

  getPosts(page: number, pagesize: number) {
    this.postsService.getPosts(page, pagesize).subscribe((res: any) => {
      this.posts = res.posts;
      this.totalPosts = res.maxPosts;
    });
  }

  onChangedPage(pageData: PageEvent) {
    this.currentPage = pageData.pageIndex + 1;
    this.postsPerPage = pageData.pageSize;

    this.getPosts(this.currentPage, this.postsPerPage);
  }
  ngOnDestroy(): void {
    this.authSubscribtion?.unsubscribe();
  }
}
